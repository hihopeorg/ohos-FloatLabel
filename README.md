# ohosFloatLabel

本项目是基于开源项目FloatLabel进行ohos化的移植和开发，可以通过项目标签以及github地址（https://github.com/IanGClifton/AndroidFloatLabel ）追踪到原项目版本

#### 项目介绍

- 项目名称：浮动标签
- 所属系列：ohos的第三方组件适配移植
- 功能：支持自定义布局、动画等。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/IanGClifton/AndroidFloatLabel
- 原项目基线版本：V1.0.4
- 编程语言：Java

#### 演示效果

![Image text](/screenshot/ohosFloatLabel.gif)

#### 安装教程

方法1.

1. 准备har包ohosFloatLabel.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'ohosFloatLabel', ext: 'har')
    ....
}
```
方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.iangclifton.ohos:floatlabel:1.0.0'
}
```

#### 使用说明

1. 布局文件定义，下面是一些使用范例：

```xml
<com.iangclifton.ohos.floatlabel.FloatLabel
    ohos:id="$+id:float_label_1"
    ohos:height="match_content"
    ohos:width="match_parent"
    ohos:hint="$string:example_label"
/>
```

2. 如果需要使用自定义布局，你可以这样：
```xml
<com.iangclifton.ohos.floatlabel.FloatLabel
    ohos:id="$+id:float_label_custom_layout_1"
    ohos:height="match_content"
    ohos:width="match_parent"
    ohos:hint="$string:example_label"
    ohos:layout="$layout:custom_float_label"
/>
```
您的自定义布局应包括标签文本视图和编辑文本。自定义布局极其有限，因为 FloatLabel 只需列出标签和 EditText，而忽略了所有其他视图。这是非常有效的，但也阻止你创建一个更复杂的布局。
下面是一个实例：
```xml
<?xml version="1.0" encoding="utf-8"?>
<StackLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_content"
    ohos:width="match_parent">

    <Text
        ohos:id="$id:float_label"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:text_color="#FF1306DD"
        ohos:text_font="bold"
        ohos:text_size="14fp"/>

    <TextField
        ohos:id="$id:edit_text"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:text_size="18fp"
        ohos:text_input_type="pattern_text"
        ohos:top_margin="20vp"
        ohos:basement="#ff00ffff"/>

</StackLayout>
```
3. 自定义动画
你也可以通过实现```FloatLabel.LabelAnimator```来自定义显示和隐藏标签的动画。请注意，您应该使用动画的alpha属性来显示和隐藏它，而不是```Component.setVisibility(int)```方法。
```java
private static class CustomLabelAnimator implements FloatLabel.LabelAnimator {
    /*package*/ static final float SCALE_X_SHOWN = 1f;
    /*package*/ static final float SCALE_X_HIDDEN = 2f;
    /*package*/ static final float SCALE_Y_SHOWN = 1f;
    /*package*/ static final float SCALE_Y_HIDDEN = 0f;

    @Override
    public void onDisplayLabel(Component label) {
        final float shift = label.getWidth() / 2;
        label.setScaleX(SCALE_X_HIDDEN);
        label.setScaleY(SCALE_Y_HIDDEN);
        label.setTranslationX(shift - label.getLeft());
        AnimatorProperty animatorProperty = new AnimatorProperty(label);
        animatorProperty.alpha(1);
        animatorProperty.scaleX(SCALE_X_SHOWN);
        animatorProperty.scaleY(SCALE_Y_SHOWN);
        animatorProperty.moveByX(-shift);
        animatorProperty.start();
    }

    @Override
    public void onHideLabel(Component label) {
        final float shift = label.getWidth() / 2;
        label.setScaleX(SCALE_X_SHOWN);
        label.setScaleY(SCALE_Y_SHOWN);
        label.setTranslationX(0 - label.getLeft());
        AnimatorProperty animatorProperty = new AnimatorProperty(label);
        animatorProperty.alpha(0);
        animatorProperty.scaleX(SCALE_X_HIDDEN);
        animatorProperty.scaleY(SCALE_Y_HIDDEN);
        animatorProperty.moveByX(shift);
        animatorProperty.start();
    }
}
```
然后再调用```setLabelAnimator```设下去:
```java
final FloatLabel floatLabel = (FloatLabel) findComponentById(ResourceTable.Id_float_label_custom_animation_1);
floatLabel.setLabelAnimator(new CustomLabelAnimator());
```
#### 版本迭代

- v1.0.0

#### 版权和许可信息

- Apache License, Version 2.0