/*
 * Copyright (C) 2014 Ian G. Clifton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iangclifton.ohos.floatlabel;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

/**
 * A ViewGroup that consists of an EditText and a TextView as the label.<br>
 * <br>
 * When the EditText is empty, its hint is displayed. When it is nonempty, the
 * TextView label is displayed.<br>
 * <br>
 * You can set the label programmatically with either
 * {@link #setLabel(String)} or {@link #setLabel(int)}.
 * 
 * @author Ian G. Clifton
 * @see <a
 *      href="http://dribbble.com/shots/1254439--GIF-Float-Label-Form-Interaction">Float
 *      label inspiration on Dribbble</a>
 */
public class FloatLabel extends StackLayout {

    private static final String SAVE_STATE_KEY_EDIT_TEXT = "saveStateEditText";
    private static final String SAVE_STATE_KEY_LABEL = "saveStateLabel";
    private static final String SAVE_STATE_PARENT = "saveStateParent";
    private static final String SAVE_STATE_TAG = "saveStateTag";
    private static final String SAVE_STATE_KEY_FOCUS = "saveStateFocus";

    /**
     * Reference to the EditText
     */
    private TextField mEditText;

    /**
     * When init is complete, child views can no longer be added
     */
    private boolean mInitComplete = false;

    /**
     * Reference to the TextView used as the label
     */
    private Text mLabel;

    /**
     * LabelAnimator that animates the appearance and disappearance of the label TextView
     */
    private LabelAnimator mLabelAnimator = new DefaultLabelAnimator();

    /**
     * True if the TextView label is showing (alpha 1f)
     */
    private boolean mLabelShowing;

    /**
     * True when any setTextWithoutAnimation method is called and then immediately turned false
     * once the text update has finished.
     */
    private boolean mSkipAnimation = false;

    /**
     * Interface for providing custom animations to the label TextView.
     */
    public interface LabelAnimator {

        /**
         * Called when the label should become visible
         *
         * @param label TextView to animate to visible
         */
        public void onDisplayLabel(Component label);

        /**
         * Called when the label should become invisible
         *
         * @param label TextView to animate to invisible
         */
        public void onHideLabel(Component label);
    }

    public FloatLabel(Context context) {
        this(context, null, null);
    }

    public FloatLabel(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public FloatLabel(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);

        setEstimateSizeListener(estimateSizeListener);
        setArrangeListener(arrangeListener);
    }

    @Override
    public void addComponent(Component child) {
        if (mInitComplete) {
            throw new UnsupportedOperationException("You cannot add child views to a FloatLabel");
        } else {
            super.addComponent(child);
        }
    }

    @Override
    public void addComponent(Component child, int index, ComponentContainer.LayoutConfig layoutConfig) {
        if (mInitComplete) {
            throw new UnsupportedOperationException("You cannot add child views to a FloatLabel");
        } else {
            super.addComponent(child, index, layoutConfig);
        }
    }

    @Override
    public void addComponent(Component child, int width, int height) {
        if (mInitComplete) {
            throw new UnsupportedOperationException("You cannot add child views to a FloatLabel");
        } else {
            super.addComponent(child, width, height);
        }
    }

    /**
     * Returns the EditText portion of this View
     * 
     * @return the EditText portion of this View
     */
    public TextField getEditText() {
        return mEditText;
    }

    /**
     * Returns the label portion of this View
     *
     * @return the label portion of this View
     */
    public Text getLabel() {
        return mLabel;
    }

    /**
     * Sets the text to be displayed above the EditText if the EditText is
     * nonempty or as the EditText hint if it is empty
     * 
     * @param resid
     *            int String resource ID
     */
    public void setLabel(int resid) {
        setLabel(getContext().getString(resid));
    }

    /**
     * Sets the text to be displayed above the EditText if the EditText is
     * nonempty or as the EditText hint if it is empty
     * 
     * @param hint
     *            CharSequence to set as the label
     */
    public void setLabel(String hint) {
        mEditText.setHint(hint);
        mLabel.setText(hint);
    }

    /**
     * Specifies a new LabelAnimator to handle calls to show/hide the label
     *
     * @param labelAnimator LabelAnimator to use; null causes use of the default LabelAnimator
     */
    public void setLabelAnimator(LabelAnimator labelAnimator) {
        if (labelAnimator == null) {
            mLabelAnimator = new DefaultLabelAnimator();
        } else {
            mLabelAnimator = labelAnimator;
        }
    }

    /**
     * Sets the EditText's text with animation
     *
     * @param resid int String resource ID
     */
    public void setText(int resid) {
        mEditText.setText(resid);
    }

    /**
     * Sets the EditText's text with label animation
     *
     * @param text CharSequence to set
     */
    public void setText(String text) {
        mEditText.setText(text);
    }

    /**
     * Sets the EditText's text without animating the label
     *
     * @param resid int String resource ID
     */
    public void setTextWithoutAnimation(int resid) {
        mSkipAnimation = true;
        mEditText.setText(resid);
    }

    /**
     * Sets the EditText's text without animating the label
     *
     * @param text String to set
     */
    public void setTextWithoutAnimation(String text) {
        mSkipAnimation = true;
        mEditText.setText(text);
    }

    ArrangeListener arrangeListener = new ArrangeListener() {
        @Override
        public boolean onArrange(int left, int top, int width, int height) {
            final int childLeft = getPaddingLeft();
            final int childRight = width - getPaddingRight();

            int childTop = getPaddingTop();
            final int childBottom = height - getPaddingBottom();

            layoutChild(mLabel, childLeft, childTop, childRight, childBottom);
            layoutChild(mEditText, childLeft, childTop + mLabel.getEstimatedHeight(), childRight, childBottom);
            return false;
        }
    };

    private void layoutChild(Component child, int parentLeft, int parentTop, int parentRight, int parentBottom) {
        if (child.getVisibility() != HIDE) {
            final LayoutConfig lp = (LayoutConfig) child.getLayoutConfig();

            final int width = child.getEstimatedWidth();
            final int height = child.getEstimatedHeight();

            int childLeft;
            final int childTop = parentTop + lp.getMarginTop();

            int gravity = lp.alignment;
            if (gravity == -1) {
                gravity = LayoutAlignment.TOP | LayoutAlignment.START;
            }

            switch (gravity) {
                case LayoutAlignment.HORIZONTAL_CENTER:
                    childLeft = parentLeft + (parentRight - parentLeft - width) / 2 + lp.getMarginLeft() - lp.getMarginRight();
                    break;
                case LayoutAlignment.END:
                    childLeft = parentRight - width - lp.getMarginRight();
                    break;
                case LayoutAlignment.START:
                default:
                    childLeft = parentLeft + lp.getMarginLeft();
            }
            child.arrange(childLeft, childTop, width, height);
        }
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            mEditText.estimateSize(widthMeasureSpec, heightMeasureSpec);
            mLabel.estimateSize(widthMeasureSpec, heightMeasureSpec);
            setEstimatedSize(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
            return false;
        }
    };

    private int measureHeight(int heightMeasureSpec) {
        int specMode = EstimateSpec.getMode(heightMeasureSpec);
        int specSize = EstimateSpec.getSize(heightMeasureSpec);

        int result = 0;
        if (specMode == EstimateSpec.PRECISE) {
            result = specSize;
        } else {
            result = mEditText.getEstimatedHeight() + mLabel.getEstimatedHeight();
            result += getPaddingTop() + getPaddingBottom();
            result = Math.max(result, getMinHeight());

            if (specMode == EstimateSpec.NOT_EXCEED) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private int measureWidth(int widthMeasureSpec) {
        int specMode = EstimateSpec.getMode(widthMeasureSpec);
        int specSize = EstimateSpec.getSize(widthMeasureSpec);

        int result = 0;
        if (specMode == EstimateSpec.PRECISE) {
            result = specSize;
        } else {
            result = Math.max(mEditText.getEstimatedWidth(), mLabel.getEstimatedWidth());
            result = Math.max(result, getMinWidth());
            result += getPaddingLeft() + getPaddingRight();
            if (specMode == EstimateSpec.NOT_EXCEED) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    /**
     * Initializes the view's default values and values from attrs, if not null
     *
     * @param context Context to access styled attributes
     * @param attrs AttributeSet from constructor or null
     * @param defStyle int resource ID of style to use for defaults
     */
    private void init(Context context, AttrSet attrs, String defStyle) {
        // Load custom attributes
        final int layout;
        int editTextId = ResourceTable.Id_edit_text;
        int floatLabelId = ResourceTable.Id_float_label;
        String text = null;
        String hint = null;
        Color hintColor = null;
        final int floatLabelColor;
        final int imeOptions;
        final int inputType;
        if (attrs == null) {
            layout = ResourceTable.Layout_float_label;
            text = null;
            hint = null;
            hintColor = null;
            floatLabelColor = 0;
            imeOptions = 0;
            inputType = 0;
        } else {
            // Main attributes
            layout = Utils.findAttr(attrs, "layout", ResourceTable.Layout_float_label);
            editTextId = Utils.getIntegerValue(attrs, "editTextId", ResourceTable.Id_edit_text);
            floatLabelId = Utils.getIntegerValue(attrs, "labelId", ResourceTable.Id_float_label);
            text = Utils.getStringValue(attrs, "text", null);
            hint = Utils.getStringValue(attrs, "hint", null);
            hintColor = Utils.getColorValue(attrs, "hint_color", new Color(Color.getIntColor("#FF666666")));
            floatLabelColor = Utils.getColorValue(attrs, "floatLabelColor", new Color(0)).getValue();
            imeOptions = Utils.getIntegerValue(attrs, "", 0);
            inputType = Utils.getIntegerValue(attrs, "text_input_type", InputAttribute.PATTERN_TEXT);

        }

        LayoutScatter.getInstance(context).parse(layout, this, true);
        mEditText = (TextField) findComponentById(editTextId);
        if (mEditText == null) {
            // fallback to default value
            mEditText = (TextField) findComponentById(ResourceTable.Id_edit_text);
        }
        if (mEditText == null) {
            throw new RuntimeException(
                    "Your layout must have an EditText whose ID is @id/edit_text");
        }
        if (editTextId != ResourceTable.Id_edit_text) {
            mEditText.setId(editTextId);
        }
        mEditText.setHint(hint);
        mEditText.setText(text);
        if (hintColor != null) {
            mEditText.setHintColor(hintColor);
        }
        if (imeOptions != 0) {
            mEditText.setInputMethodOption(imeOptions);
        }
        if (inputType != 0) {
            mEditText.setTextInputType(inputType);
        }

        // Set up the label view
        mLabel = (Text) findComponentById(floatLabelId);
        if (mLabel == null) {
            // fallback to default value
            mLabel = (Text) findComponentById(ResourceTable.Id_float_label);
        }
        if (mLabel == null) {
            throw new RuntimeException(
                    "Your layout must have a TextView whose ID is @id/float_label");
        }
        if (floatLabelId != ResourceTable.Id_float_label) {
            mLabel.setId(floatLabelId);
        }
        mLabel.setText(mEditText.getHint());
        if (floatLabelColor != 0)
            mLabel.setTextColor(new Color(floatLabelColor));

        // Listen to EditText to know when it is empty or nonempty
        mEditText.addTextObserver(new EditTextWatcher());

        // Check current state of EditText
        if (mEditText.getText().length() == 0) {
            mLabel.setAlpha(0);
            mLabelShowing = false;
        } else {
            mLabel.setVisibility(Component.VISIBLE);
            mLabelShowing = true;
        }

        // Mark init as complete to prevent accidentally breaking the view by
        // adding children
        mInitComplete = true;
        mEditText.setFocusChangedListener(new FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean hasFocus) {
                ShapeElement element = new ShapeElement();
                if (hasFocus) {
                    element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#00B2EE")));
                } else {
                    element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#BABABA")));
                }
                mEditText.setBasement(element);
            }
        });
    }

    /**
     * LabelAnimator that uses the traditional float label Y shift and fade.
     *
     * @author Ian G. Clifton
     */
    private static class DefaultLabelAnimator implements LabelAnimator {

        @Override
        public void onDisplayLabel(Component label) {
            final float offset = label.getHeight() / 2;
            final float currentY = label.getTop() + label.getTranslationY();
            if (Math.abs(currentY - offset) > .0000001) {
                label.setTranslationY(offset - label.getTop());
            }
            AnimatorProperty animatorProperty = new AnimatorProperty(label);
            animatorProperty.alpha(1);
            animatorProperty.moveByY(-offset);
            animatorProperty.start();
        }

        @Override
        public void onHideLabel(Component label) {
            final float offset = label.getHeight() / 2;
            final float currentY = label.getTop() + label.getTranslationY();
            if (currentY != 0) {
                label.setTranslationY(0 - label.getTop());
            }
            AnimatorProperty animatorProperty = new AnimatorProperty(label);
            animatorProperty.alpha(0);
            animatorProperty.moveByY(offset);
            animatorProperty.start();
        }
    }
    /**
     * TextWatcher that notifies FloatLabel when the EditText changes between
     * having text and not having text or vice versa.
     * 
     * @author Ian G. Clifton
     */
    private class EditTextWatcher implements Text.TextObserver{
        @Override
        public void onTextUpdated(String text, int start, int before, int count) {
            if (mSkipAnimation) {
                mSkipAnimation = false;
                if (text.length() == 0) {
                    // TextView label should be gone
                    if (mLabelShowing) {
                        mLabel.setAlpha(0);
                        mLabelShowing = false;
                    }
                } else if (!mLabelShowing) {
                    // TextView label should be visible
                    mLabel.setAlpha(1);
                    mLabel.setTranslationY(0 - mLabel.getTop());
                    mLabelShowing = true;
                }
                return;
            }
            if (text.length() == 0) {
                // Text is empty; TextView label should be invisible
                if (mLabelShowing) {
                    mLabelAnimator.onHideLabel(mLabel);
                    mLabelShowing = false;
                }
            } else if (!mLabelShowing) {
                // Text is nonempty; TextView label should be visible
                mLabelShowing = true;
                mLabelAnimator.onDisplayLabel(mLabel);
            }
        }
    }
}
