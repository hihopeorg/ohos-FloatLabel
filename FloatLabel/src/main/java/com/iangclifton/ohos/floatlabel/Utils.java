/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iangclifton.ohos.floatlabel;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

import java.util.NoSuchElementException;
import java.util.Optional;

public final class Utils {

  public static int getIntegerValue(AttrSet attrSet, String name, int defaultValue) {
    if (attrSet == null) {
      return defaultValue;
    }
    try {
      int value = attrSet.getAttr(name).get().getIntegerValue();
      return value;
    } catch (NoSuchElementException e) {
      return defaultValue;
    }
  }

  public static String getStringValue(AttrSet attrSet, String name, String defaultValue) {
    if (attrSet == null) {
      return defaultValue;
    }
    try {
      String value = attrSet.getAttr(name).get().getStringValue();
      return value;
    } catch (NoSuchElementException e) {
      return defaultValue;
    }
  }

  public static Color getColorValue(AttrSet attrSet, String name, Color defaultColor) {
    if (attrSet == null) {
      return defaultColor;
    }
    try {
      Color color = attrSet.getAttr(name).get().getColorValue();
      return color;
    } catch (NoSuchElementException e) {
      return defaultColor;
    }
  }

  public static int findAttr(AttrSet attrs, String name, int defaultValue) {
    if (attrs == null) {
      return defaultValue;
    }
    Optional<Attr> attr = attrs.getAttr(name);
    try {
      String stringValue = attr.get().getStringValue();
      if (stringValue.contains("$layout")) {
        String layout = stringValue.substring(stringValue.indexOf(":") + 1, stringValue.length());
        return Integer.parseInt(layout);
      }
      return defaultValue;
    } catch (NoSuchElementException e) {
      return defaultValue;
    }
  }
}
