package com.iangclifton.ohos.floatlabelexample.slice;

import com.iangclifton.ohos.floatlabel.FloatLabel;
import com.iangclifton.ohos.floatlabelexample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        final FloatLabel floatLabel = (FloatLabel) findComponentById(ResourceTable.Id_float_label_custom_animation_1);
        floatLabel.setLabelAnimator(new CustomLabelAnimator());
    }

    /**
     * LabelAnimator that uses a custom X shift and fade.
     *
     * @author Ian G. Clifton
     */
    private static class CustomLabelAnimator implements FloatLabel.LabelAnimator {
        /*package*/ static final float SCALE_X_SHOWN = 1f;
        /*package*/ static final float SCALE_X_HIDDEN = 2f;
        /*package*/ static final float SCALE_Y_SHOWN = 1f;
        /*package*/ static final float SCALE_Y_HIDDEN = 0f;

        @Override
        public void onDisplayLabel(Component label) {
            final float shift = label.getWidth() / 2;
            label.setScaleX(SCALE_X_HIDDEN);
            label.setScaleY(SCALE_Y_HIDDEN);
            label.setTranslationX(shift - label.getLeft());
            AnimatorProperty animatorProperty = new AnimatorProperty(label);
            animatorProperty.alpha(1);
            animatorProperty.scaleX(SCALE_X_SHOWN);
            animatorProperty.scaleY(SCALE_Y_SHOWN);
            animatorProperty.moveByX(-shift);
            animatorProperty.start();
        }

        @Override
        public void onHideLabel(Component label) {
            final float shift = label.getWidth() / 2;
            label.setScaleX(SCALE_X_SHOWN);
            label.setScaleY(SCALE_Y_SHOWN);
            label.setTranslationX(0 - label.getLeft());
            AnimatorProperty animatorProperty = new AnimatorProperty(label);
            animatorProperty.alpha(0);
            animatorProperty.scaleX(SCALE_X_HIDDEN);
            animatorProperty.scaleY(SCALE_Y_HIDDEN);
            animatorProperty.moveByX(shift);
            animatorProperty.start();
        }
    }
}
