package com.iangclifton.ohos.floatlabelexample.junit;

import com.iangclifton.ohos.floatlabel.FloatLabel;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Clock;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class FloatLabelTest {

    @Test
    public void testAddComponent() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatLabel floatLabel = new FloatLabel(context);
        Field field = FloatLabel.class.getDeclaredField("mInitComplete");
        field.setAccessible(true);
        field.set(floatLabel,false);
        int childCount = floatLabel.getChildCount();
        floatLabel.addComponent(new Component(context));
        int childCount1 = floatLabel.getChildCount();
        Assert.assertEquals(childCount+1,childCount1);
    }

    @Test
    public void testGetEditText() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatLabel floatLabel = new FloatLabel(context);
        String str = "testText";
        floatLabel.setTextWithoutAnimation(str);
        TextField editText = floatLabel.getEditText();
        Assert.assertEquals(editText.getText(),str);
    }

    @Test
    public void testGetLabel() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatLabel floatLabel = new FloatLabel(context);
        String str = "testLabel";
        floatLabel.setLabel(str);
        Assert.assertEquals(floatLabel.getLabel().getText(),str);
    }

    @Test
    public void testSetLabelAnimator() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatLabel floatLabel = new FloatLabel(context);
        FloatLabel.LabelAnimator labelAnimator = new FloatLabel.LabelAnimator() {
            @Override
            public void onDisplayLabel(Component label) {
            }

            @Override
            public void onHideLabel(Component label) {
            }
        };
        floatLabel.setLabelAnimator(labelAnimator);
        Field field = FloatLabel.class.getDeclaredField("mLabelAnimator");
        field.setAccessible(true);
        FloatLabel.LabelAnimator mLabelAnimator = (FloatLabel.LabelAnimator)field.get(floatLabel);
        Assert.assertEquals(labelAnimator,mLabelAnimator);
    }

    @Test
    public void testSetText() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatLabel floatLabel = new FloatLabel(context);
        String str = "testText";
        floatLabel.setText(str);
        Assert.assertEquals(floatLabel.getEditText().getText(),str);
    }
}