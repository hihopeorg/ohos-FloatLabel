package com.iangclifton.ohos.floatlabelexample.uiTest;

import com.iangclifton.ohos.floatlabel.FloatLabel;
import com.iangclifton.ohos.floatlabelexample.MainAbility;
import com.iangclifton.ohos.floatlabelexample.ResourceTable;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.io.IOException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private MainAbility mainAbility;

    @Before
    public void before() {
        mainAbility = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(mainAbility, 5);
    }

    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String x) {
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01View() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_1);
        FloatLabel floatLabel2 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_2);
        FloatLabel floatLabel3 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_3);
        FloatLabel floatLabel4 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_4);
        FloatLabel floatLabel5 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_5);
        Assert.assertNotNull("第一个floatLabel显示失败", floatLabel1);
        Assert.assertNotNull("第二个floatLabel显示失败", floatLabel2);
        Assert.assertNotNull("第三个floatLabel显示失败", floatLabel3);
        Assert.assertNotNull("第四个floatLabel显示失败", floatLabel4);
        Assert.assertNotNull("第五个floatLabel显示失败", floatLabel5);
        FloatLabel floatLabel6 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_custom_layout_1);
        FloatLabel floatLabel7 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_custom_animation_1);
        Assert.assertNotNull("第六个floatLabel显示失败", floatLabel6);
        Assert.assertNotNull("第七个floatLabel显示失败", floatLabel7);
    }

    @Test
    public void test02first() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_1);
        TextField editText = floatLabel1.getEditText();
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        addText(editText);
        float alpha1 = label.getAlpha();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        Assert.assertTrue("输入文本后标签未显示", alpha != alpha1 && alpha1 != alpha2);
        stopThread(1000);
        removeText(editText);
        float alpha3 = label.getAlpha();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        Assert.assertTrue("删除文本后标签未隐藏", alpha3 != alpha2 && alpha4 == alpha);
        stopThread(1000);
    }

    @Test
    public void test03second() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_2);
        TextField editText = floatLabel1.getEditText();
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        removeText(editText);
        float alpha1 = label.getAlpha();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        Assert.assertTrue("删除文本后标签未隐藏", alpha != alpha1 && alpha1 != alpha2);
        stopThread(1000);
        addText(editText);
        float alpha3 = label.getAlpha();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        Assert.assertTrue("输入文本后标签未显示", alpha3 != alpha2 && alpha4 == alpha);
        stopThread(1000);
    }

    @Test
    public void test04third() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_3);
        TextField editText = floatLabel1.getEditText();
        Color hintColor = editText.getHintColor();
        int color = Color.getIntColor("#99666666");
        Assert.assertTrue("提示信息颜色显示异常", hintColor.getValue() == color);
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        addText(editText);
        float alpha1 = label.getAlpha();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        Assert.assertTrue("输入文本后标签未显示", alpha != alpha1 && alpha1 != alpha2);
        stopThread(1000);
        removeText(editText);
        float alpha3 = label.getAlpha();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        Assert.assertTrue("删除文本后标签未隐藏", alpha3 != alpha2 && alpha4 == alpha);
        stopThread(1000);
    }

    @Test
    public void test05fourth() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_4);
        TextField editText = floatLabel1.getEditText();
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        addText(editText);
        float alpha1 = label.getAlpha();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        Assert.assertTrue("输入文本后标签未显示", alpha != alpha1 && alpha1 != alpha2);
        Color textColor = label.getTextColor();
        int color = Color.getIntColor("#FF0000");
        Assert.assertTrue("标签颜色显示异常", textColor.getValue() == color);
        stopThread(1000);
        removeText(editText);
        float alpha3 = label.getAlpha();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        Assert.assertTrue("删除文本后标签未隐藏", alpha3 != alpha2 && alpha4 == alpha);
        stopThread(1000);
    }

    @Test
    public void test06fifth() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_5);
        TextField editText = floatLabel1.getEditText();
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        addText(editText);
        float alpha1 = label.getAlpha();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        Assert.assertTrue("输入文本后标签未显示", alpha != alpha1 && alpha1 != alpha2);
        int textInputType = editText.getTextInputType();
        Assert.assertTrue("文本类型不为密码", textInputType == 7);
        stopThread(1000);
        removeText(editText);
        float alpha3 = label.getAlpha();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        Assert.assertTrue("删除文本后标签未隐藏", alpha3 != alpha2 && alpha4 == alpha);
        stopThread(1000);
    }

    @Test
    public void test07sixth() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_custom_layout_1);
        TextField editText = floatLabel1.getEditText();
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        addText(editText);
        float alpha1 = label.getAlpha();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        Assert.assertTrue("输入文本后标签未显示", alpha != alpha1 && alpha1 != alpha2);
        Color textColor = label.getTextColor();
        int color = Color.getIntColor("#FF1306DD");
        Assert.assertTrue("标签颜色显示异常", textColor.getValue() == color);
        stopThread(1000);
        removeText(editText);
        float alpha3 = label.getAlpha();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        Assert.assertTrue("删除文本后标签未隐藏", alpha3 != alpha2 && alpha4 == alpha);
        stopThread(1000);
    }

    @Test
    public void test08seventh() {
        stopThread(2000);
        FloatLabel floatLabel1 = (FloatLabel) mainAbility.findComponentById(ResourceTable.Id_float_label_custom_animation_1);
        TextField editText = floatLabel1.getEditText();
        Text label = floatLabel1.getLabel();
        float alpha = label.getAlpha();
        float scaleX = label.getScaleX();
        float scaleY = label.getScaleY();
        addText(editText);
        float alpha1 = label.getAlpha();
        float scaleX1 = label.getScaleX();
        float scaleY1 = label.getScaleY();
        stopThread(2000);
        float alpha2 = label.getAlpha();
        float scaleX2 = label.getScaleX();
        float scaleY2 = label.getScaleY();
        Assert.assertTrue("输入文本后标签未显示", alpha != alpha1 && alpha1 != alpha2);
        Assert.assertTrue("输入文本后标签未横向放大", scaleX != scaleX1 && scaleY != scaleY1 && scaleX2 < scaleX1 && scaleY2 > scaleY1);
        stopThread(1000);
        removeText(editText);
        float alpha3 = label.getAlpha();
        float scaleX3 = label.getScaleX();
        float scaleY3 = label.getScaleY();
        stopThread(2000);
        float alpha4 = label.getAlpha();
        float scaleX4 = label.getScaleX();
        float scaleY4 = label.getScaleY();
        Assert.assertTrue("删除文本后标签未隐藏", alpha3 != alpha2 && alpha4 == alpha);
        Assert.assertTrue("删除文本后标签未横向缩小", scaleX3 < scaleX4 && scaleY3 > scaleY4 && scaleX2 < scaleX3 && scaleY2 > scaleY3);
        stopThread(1000);
    }


    public void addText(TextField editText) {
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText("This");
        });
        stopThread(50);
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText(editText.getText() + " is");
        });
        stopThread(50);
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText(editText.getText() + " a");
        });
        stopThread(50);
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText(editText.getText() + " Test.");
        });
        stopThread(50);
    }

    public void removeText(TextField editText) {
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText(editText.getText().substring(0, editText.getText().length() / 3 * 2));
        });
        stopThread(150);
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText(editText.getText().substring(0, editText.getText().length() / 2));
        });
        stopThread(150);
        mainAbility.getUITaskDispatcher().asyncDispatch(() -> {
            editText.setText("");
        });
        stopThread(200);

    }


}